import React, { Component } from 'react';

import {school} from '../models.js';
import School from './School.jsx';


class SchoolDashboard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="col-md-7 tile">
                <h3>
                    Click on a school to edit
                </h3>
                <hr/>
                { this.props.schools.length !== 0 &&
                    <section>
                        {   this.props.schools.map((school)=> {
                                return <School key={school.id} school={school} />
                            })
                        }
                    </section>
                }
            </div>
        );
    }
}


export default SchoolDashboard;
