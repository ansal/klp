import React, { Component } from 'react';

import {titleCase} from '../utils.js';
import {school} from '../models.js';

class School extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: this.props.school.name,
            editedData: null,
            editing: false
        }

        let editedData = school.getNamefromLocalStorage(this.props.school.id);
        if(editedData) {
            this.state.name = editedData.name;
            this.state.editedData = editedData;
        }

        this.editName = this.editName.bind(this);
        this.handleNameChanged = this.handleNameChanged.bind(this);
    }

    editName(e) {
        e.preventDefault();
        this.setState({editing: !this.state.editing});
    }

    handleNameChanged(e) {
        this.setState({name: e.target.value});
        school.saveNameToLocalStorage(
            this.props.school.id, e.target.value
        );
    }

    render() {
        return (
            <div className="list-group">
                <div href="#" className="list-group-item">
                    <h4 className="list-group-item-heading">
                        <a href="#" onClick={this.editName}>
                            {this.state.name}
                        </a>
                    </h4>
                    <p className="list-group-item-text">
                        {this.props.school.type.name} at &nbsp;
                        {titleCase(this.props.school.admin3)}, &nbsp;
                        {titleCase(this.props.school.admin2)}, &nbsp;
                        {titleCase(this.props.school.admin1)}.
                    </p>
                    {
                        this.state.editing &&
                            <p>
                                <br/>
                                <input type="text" value={this.state.name} className="form-control" onChange={this.handleNameChanged}/>
                            </p>
                    }
                    {
                        this.state.editedData && 
                            <p>
                                <small className="text-warning">
                                    You have edited this school on {new Date(this.state.editedData.time).toDateString().toString().slice(0, 21)}. The original name was "{this.props.school.name}"
                                </small>
                            </p>
                    }
                </div>
            </div>
        );
    }
}


export default School;
