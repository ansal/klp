import React, { Component } from 'react';


class WelcomeDashboard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="col-md-7 tile">
                <h3 className="pull-right">
                    Welcome to KLP!
                </h3>
                <div className="row"></div>
                <hr/>
                <div className="row"></div>
                <p>
                    Click any district under "Browse Schools" to see blocks, clusters and schools under them.
                </p>
                <p>
                    To see visualization, select any link under "Visualizations".
                </p>
                <p></p>
            </div>
        );
    }
}


export default WelcomeDashboard;
