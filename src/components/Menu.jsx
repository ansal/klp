import React, { Component } from 'react';

import {titleCase} from '../utils.js';
import {school} from '../models.js';
import WelcomeDashboard from './WelcomeDashboard.jsx';
import SchoolDashboard from './SchoolDashboard.jsx';
import VisualizationDashboard from './VisualizationDashboard.jsx';

class SchoolMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            districts: [],
            blocks: [],
            clusters: [],
            schools: [],
            
            districtSelected: null,
            blockSelected: null,
            clusterSelected: null,
            visualizationSelected: null
        };

        this.districtSelected = this.districtSelected.bind(this);
        this.blockSelected = this.blockSelected.bind(this);
        this.clusterSelected = this.clusterSelected.bind(this);
    }

    componentDidMount() {
        // Fetch the schools and populate the state
        school.fetchSchools((schools)=> {
            this.setState({
                districts: school.getDistricts()
            });
        });
    }

    districtSelected(district, e) {
        e.preventDefault();
        this.setState({
            districtSelected: district,
            blocks: school.getBlocks(district),
            clusters: []
        });
    }

    blockSelected(block, e) {
        e.preventDefault();
        this.setState({
            blockSelected: block,
            clusters: school.getClusters(block)
        });
    }

    clusterSelected(cluster, e) {
        e.preventDefault();
        this.setState({
            clusterSelected: cluster,
            schools: school.getSchools(cluster),
            visualizationSelected: null
        });
    }

    visualizationClicked(visualizationSelected, e) {
        e.preventDefault();
        this.setState({
            blocks: [],
            clusters: [],
            schools: [],
            visualizationSelected: visualizationSelected
        });
    }

    renderClusters() {
        return this.state.clusters.map((cluster) => {
            return (
                <li key={cluster}>
                    <a href="#" onClick={(e) => this.clusterSelected(
                            cluster, e)}>
                        {titleCase(cluster)}
                    </a>
                </li>
            );
        });
    }

    renderBlocks() {
        return this.state.blocks.map((block) => {
            return (
                <li key={block}>
                    <a href="#" onClick={(e) => this.blockSelected(
                            block, e)}>
                        {titleCase(block)}
                    </a>
                    {this.state.blockSelected === block && <ul>
                            {this.renderClusters()}
                        </ul>
                    }
                </li>
            );
        });
    }

    renderDistricts() {
        return this.state.districts.map((district) => {
            return (
                <li key={district}>
                    <a href="#" onClick={(e) => this.districtSelected(
                            district, e)}>
                        {titleCase(district)}
                    </a>
                    {this.state.districtSelected === district && <ul>
                            {this.renderBlocks()}
                        </ul>
                    }
                </li>
            );
        })
    }

    renderDashboard() {
        if (this.state.schools.length !== 0 && 
                !this.state.visualizationSelected) {
            return <SchoolDashboard schools={this.state.schools} />
        } else if(this.state.visualizationSelected === 'language') {
            return <VisualizationDashboard defaultSelected='all' chartType="language"/>
        } else if(this.state.visualizationSelected === 'level') {
            return <VisualizationDashboard defaultSelected='all' chartType="level"/>
        } else {
            return <WelcomeDashboard/>
        }
    }

    render() {
        return (
            <section>
                <div className="col-md-offset-1 col-md-3 tile">
                    <h3>
                        Browse Schools
                        <hr/>
                    </h3>
                    <ul>
                        {this.renderDistricts()}
                    </ul>
                    <h3>
                        Visualization
                        <hr/>
                    </h3>
                    <ul>
                        <li>
                            <a href="#" onClick={(e) => this.visualizationClicked('language', e)}>Borrowers by language</a>
                        </li>
                        <li>
                            <a href="#" onClick={(e) => this.visualizationClicked('level', e)}>Borrowers by level</a>
                        </li>
                    </ul>
                    <div className="row">
                        <p></p>
                    </div>
                </div>
                { this.renderDashboard() }
            </section>
        );
    }
}


export default SchoolMenu;
