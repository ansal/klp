import React, { Component } from 'react';

import Menu from './Menu.jsx';

class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="container-fluid main-container">
                <div className="row">
                    <Menu />
                </div>
            </div>
        );
    }
}


export default App;
