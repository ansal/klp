import React, { Component } from 'react';
import {BarChart,
            Cell,
            Bar,
            XAxis,
            YAxis,
            CartesianGrid,
            Tooltip,
            Legend,
            ResponsiveContainer
        } from 'recharts';

import {titleCase} from '../utils.js';
import {visualData} from '../models.js';


class VisualizationDashboard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            chartData: [],
            districts: []
        };

        visualData.fetchData(()=> {            
            this.setChartData(this.props.chartType, 'all');
        });

        this.selectDistrict = this.selectDistrict.bind(this);
        this.clearSelectDistrict = this.clearSelectDistrict.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.setChartData(nextProps.chartType, 'all');
        this.clearSelectDistrict();
    }

    setChartData(chartType, district) {
        this.setState({
            districts: visualData.getDistricts(chartType),
            chartData: visualData.getChartData(chartType, district)
        });
    }

    selectDistrict(e) {
        this.setChartData(this.props.chartType, e.target.value);
    }

    clearSelectDistrict() {
        this.districtSelect.value = 'all';
    }    

    render() {
        let colors = {
            'WHITE': 'gray',
            'YELLOW': 'yellow',
            'RED': 'red',
            'GREEN': 'green',
            'ORANGE': 'orange',
            'BLUE': 'blue'
        };

        let districts = this.state.districts.map(
            (d) => {
                return <option key={d} value={d}>{titleCase(d)}</option>;
            }
        );

        return (
            <div className="col-md-7 tile">
                <h3>
                    Borrowing patterns by {this.props.chartType}
                </h3>
                <hr/>
                <div className="row"></div>
                <select ref={(select) => { this.districtSelect = select; }} className="form-control" onChange={this.selectDistrict}>
                    <option value="all">All Districts</option>
                    {districts}
                </select>
                <div className="row">
                    <p></p>
                </div>
                <ResponsiveContainer height={420}>
                    <BarChart
                            layout="vertical"
                            width={600}
                            height={300}
                            data={this.state.chartData}
                            margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                        <XAxis type="number"/>
                        <YAxis type="category" dataKey="name"/>
                        <CartesianGrid strokeDasharray="3 3"/>
                        <Tooltip/>
                        <Legend />
                        <Bar dataKey="borrowings">
                            {
                                this.state.chartData.map((entry, index) => (
                                    <Cell key={index} fill={colors[entry.name] ? colors[entry.name] : '#82ca9d'}/>
                                ))
                            }
                        </Bar>
                    </BarChart>
                </ResponsiveContainer>
            </div>
        );
    }
}


export default VisualizationDashboard;
