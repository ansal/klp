// A simple model for School
import fetch from 'node-fetch';

import CONFIG from './config.js';

class School {
    constructor() {
        this.schools = [];
    }

    fetchSchools(cb) {
        fetch(CONFIG.dataUrl + 'schools.json')
            .then(res => res.json())
            .then(json => {
                this.schools = json.features;
                if (cb) {
                    cb(json.features);
                }
            });
    }

    getDistricts() {
        let districts = this.schools.map((school)=> {
            return school.admin1;
        });
        return [ ...new Set(districts) ];
    }

    getBlocks(district) {
        let blocks = this.schools.filter((school)=> {
            return school.admin1 === district;
        }).map((school) => school.admin2);
        return [ ...new Set(blocks) ];   
    }

    getClusters(block) {
        let clusters = this.schools.filter((school)=> {
            return school.admin2 === block;
        }).map((school) => school.admin3);
        return [ ...new Set(clusters) ];   
    }

    getSchools(cluster) {
        let schools = this.schools.filter((school)=> {
            return school.admin3 === cluster;
        });
        return schools;
    }

    getNamefromLocalStorage(id) {
        let data = localStorage.getItem(id);
        if(data) {
            return JSON.parse(data);
        } else {
            return null;
        }
    }

    saveNameToLocalStorage(id, name) {
        localStorage.setItem(
            id,
            JSON.stringify({
                name: name,
                time: new Date()
            })
        );
    }
}


class Visualization {
    constructor() {
        this.data = {};
    }

    fetchData(cb) {
        fetch(CONFIG.dataUrl + 'visualization.json')
            .then(res => res.json())
            .then(json => {
                this.data = json;
                if (cb) {
                    cb(this.data);
                }
            });
    }

    getChartData(chartType, district) {
        let chartData = [];
        for(let i in this.data[chartType]) {
            let sum = this.data[chartType][i][district] ? this.data[chartType][i][district] : 0;
            chartData.push({
                name: i,
                borrowings: sum
            });
        }
        chartData.sort((a, b) => b.borrowings - a.borrowings);
        return chartData;
    }

    getDistricts(chartType) {
        let districts = [];
        for(let i in this.data[chartType]) {
            for(let d in this.data[chartType][i]) {
                if(d !== 'all') {
                    districts.push(d);
                }
            }
        }
        districts.sort((a, b) => a < b ? -1 : 1);
        return [ ...new Set(districts) ];
    }
}


var school = new School(),
    visualData = new Visualization();

export {
    school,
    visualData
};