// Various utilities for KLP app

var titleCase = (s) => s.charAt(0).toUpperCase() + s.slice(1);


module.exports = {
    titleCase: titleCase
}
