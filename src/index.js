import React from 'react';
import {render} from 'react-dom';
import App from './components/App.jsx';

// Import bootstrap
import '../css/bootstrap/css/bootstrap.min.css';
import '../css/styles.css';

// School model
import School from './models.js';


let startApp = ()=> {
    render(
        <App/>,
        document.getElementById('app')
    );
};

startApp();
