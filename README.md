KLP - School browser and visualization
======================================

## Setting up the app

Assuming you have [Node.js](https://nodejs.org) and the package manager
that comes with it - [npm](https://www.npmjs.com/) installed, head to the command line and run the following commands to setup the project - 

## Clone the app and install al the required packages

```
git clone https://gitlab.com/ansal/klp
cd klp
npm install
npm run dev
```

This will open a development server at [http://localhost:3000](http://localhost:3000)

## Build the app
```
npm run build
```

This will bundle the app inside bundle/ folder.
