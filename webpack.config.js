module.exports = {
    context: __dirname,

    entry: {
        javascript: './src/index.js',
    },

    output: {
        filename: 'bundle.js',
        path: __dirname + '/dist',
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                'babel-loader'
                ]
            },
            {test: /\.jsx$/, loader: 'babel-loader'},
            {test: /\.woff|\.woff2|\.svg|.eot|\.ttf|\.png|\.gif|\.ico|\.html/, loader: 'file-loader'},
            {test: /\.json$/, loader: 'file-loader'},
            {test: /\.css$/, loader: "style-loader!css-loader"},
            {test: /\.scss$/, loader: 'style-loader!css-loader!sass'},
        ]
    },

    resolve: {
        extensions: ['.js', '.json', '.css', '.jsx', '.json']
    },

    devServer: {
        port: 3000,
        compress: false,
        historyApiFallback: true,
        inline: true,
        publicPath: '/dist/',
        stats: {
            colors: true,
            timings: true,
            version: true,
            warnings: true
        }
    }

};