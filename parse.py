# Parse CSV files to get district wise data for lang and level
import csv


SCHOOL_DICT = {}
LANG_RESULT = {}
LEVEL_RESULT = {}


def read_data(f, delimiter):
    """ Reads a CSV file """
    with open(f, 'rb') as csvfile:
        data = []
        csv_data = csv.reader(csvfile, delimiter=delimiter)
        for c in csv_data:
            data.append(c)
        return data


def prepare_school_hash(schools):
    """ Stores school list by id on a dict
        for quicker access.
    """
    d = {}
    for s in schools:
        d[str(s[0])] = s
    return d


def calculate_feed_count(data):
    """ Calculates lang sum for each district """
    calculation = {}

    for d in data:
        if d[0] in SCHOOL_DICT:
            district = SCHOOL_DICT[d[0]][8]
            field = d[4]
            count = d[5]

            if field not in calculation:
                calculation[field] = {
                    'all': 0
                }

            calculation[field]['all'] += int(count)

            if district not in calculation[field]:
                calculation[field][district] = 0
            calculation[field][district] += int(count)

    return calculation


if __name__ == '__main__':

    # Read all schools in a dict
    SCHOOL_DICT = prepare_school_hash(read_data('primaryschool.csv', ';')[1:])

    # Calculate lang sum for each district
    LANG_RESULT = calculate_feed_count(read_data('lib_lang.csv', ',')[1:])

    # Calculate level sum for each district
    LEVEL_RESULT = calculate_feed_count(read_data('lib_level.csv', ',')[1:])

    print {
        'language': LANG_RESULT,
        'level': LEVEL_RESULT
    }
